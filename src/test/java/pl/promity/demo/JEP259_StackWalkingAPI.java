package pl.promity.demo;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

import static pl.promity.demo.Logger.log;

public class JEP259_StackWalkingAPI {

    @Test
    public void pre_java9_stackTraceTest() {
        Arrays.stream(Thread.currentThread().getStackTrace()).forEach(
                element -> log("%25s | %5d", element.getMethodName(), element.getLineNumber())
        );
    }

    @Test
    public void java9_stackWalkingTest() {
        // cannot iterate inside walk function, have to collect stream to list
        // need to add StackWalker.Option.RETAIN_CLASS_REFERENCE option in order to get class name
        StackWalker.getInstance(
            Set.of(
                StackWalker.Option.RETAIN_CLASS_REFERENCE
            )).walk(
                stackFrameStream -> stackFrameStream
                        .filter( frame -> frame.getClassName().startsWith("org.junit"))
                        .limit(10)
                        .collect(Collectors.toList())
            ).forEach(
                    element -> log("%60s | %5d", element.getClassName(), element.getLineNumber())
            );
    }

}
