package pl.promity.demo;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

import static pl.promity.demo.Logger.log;

public class JEP213_MillingProjectCoinTest {

    class Underscore {

        // Nope
//        private String _;

        // Ok
        private String __;

    }

    interface ComputationOne {

        // part of public API
        default long compute(){
            return computePart() * 11;
        }

        // part of public API
        private long computePart(){
            return 11*11;
        }

        long computeSpecific();

    }

    interface ComputationTwo {

        // part of public API
        default long compute(){
            return computePart() * 22;
        }

        // part of public API
        private long computePart(){
            return 22 * 22;
        }

        // part of public API
        default long computeTwo(){
            return computePart() * 222;
        }

        long computeSpecific();

    }

    class ComputationImpl implements ComputationOne, ComputationTwo{

        // must be overridden, because of method conflicts
        @Override
        public long compute() {
            return ComputationOne.super.compute() * ComputationTwo.super.compute();
        }

        @Override
        public long computeSpecific() {
            return 44;
        }
    }

    @Test
    public void java9_interfacePrivateMethodsTest() {

        ComputationTwo two = new ComputationImpl();

        log("two.compute                : %s", two.compute());
        log("two.computeTwo             : %s", two.computeTwo());
        log("two.computeSpecific        : %s", two.computeSpecific());

        ComputationOne one = new ComputationImpl();

        log("one.compute                : %s", one.compute());
        log("one.computeSpecific        : %s", one.computeSpecific());

    }


    @Test
    public void java9_anonymousClassDiamondTest() {

        // no need for AbstractClassDemo<String>
        AbstractClassDemo<String> object = new AbstractClassDemo<>() {
            @Override
            public String introduce() {
                return "hello, I'm no diamond";
            }
        };

        log(object.introduce());

    }

    abstract class AbstractClassDemo<T> {

        public abstract T introduce();

    }

    @Test
    public void pre_java9_tryWithResourceTest() throws Exception {

        try(BufferedReader input = new BufferedReader(new FileReader(getDemoFile()))) {
            input.lines().forEach(
                line -> log(line)
            );
        }

    }

    private File getDemoFile() throws URISyntaxException {
        URL url = this.getClass().getClassLoader().getResource("demo.txt");
        return Paths.get(url.toURI()).toFile();
    }

    @Test
    public void java9_tryWithResourceTest() throws Exception {
        // effectively final
        BufferedReader input = new BufferedReader(new FileReader(getDemoFile()));
        try(input) {
            input.lines().forEach(
                line -> log(line)
            );
        }
        // check if input is closed
        assertThatExceptionOfType(IOException.class).isThrownBy(
            input::read
        );
    }


}
