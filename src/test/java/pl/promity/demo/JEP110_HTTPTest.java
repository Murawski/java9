package pl.promity.demo;

import com.google.common.base.Strings;
//import jdk.incubator.http.HttpResponse;
import org.junit.Test;

import java.net.URI;
import java.util.concurrent.CompletableFuture;

import static pl.promity.demo.Logger.log;

/**
 * This test won't run in intellij, run it through command line
 * Run:
 *  - 'export JAVA_HOME=/usr/lib/jvm/jdk-9.0.4/'
 *  - 'mvn -Dtest=pl.promity.demo.JEP110_HTTPTest test'
 */
public class JEP110_HTTPTest {

//    @Test
//    public void java9_webSocketClientTest() throws Exception {
//        WebSocketDemoClient client = new WebSocketDemoClient();
//
//        // connect do demo websocket echo server
//        client.connect(URI.create("ws://demos.kaazing.com/echo"));
//
//        // send first message
//        String firstMessage = "My first message";
//        log("Sending first message");
//        client.sendNonBlockingMessage(firstMessage)
//                .thenAccept( x -> log("Finished sending message     : %s", firstMessage) );
//
//        // send second message
//        String secondMessage = "Arka gdynia kurwa świnia";
//        log("Sending second message");
//
//        client.sendNonBlockingMessage(secondMessage)
//                .thenAccept( x -> log("Finished sending message     : %s", secondMessage) );
//
//        // close connection
//        client.closeSocket();
//    }

//    @Test
//    public void java9_HTTPClientTest() throws Exception {
//        // create client and server address objects
//        HTTPDemoClient client = new HTTPDemoClient();
//        URI echoServerURI = new URI("http://postman-echo.com/post");
//
//        // send two requests: long and short and print results from first received response
//        CompletableFuture<HttpResponse<String>> longNonBlockingResponse = client.sendNonBlockingMessage(echoServerURI, Strings.repeat("Long non blocking message ", 2000));
//        CompletableFuture<HttpResponse<String>> shortNonBlockingResponse = client.sendNonBlockingMessage(echoServerURI, "Short non blocking message");
//        longNonBlockingResponse.acceptEitherAsync(shortNonBlockingResponse, response -> {
//            log("First non blocking response version          : %s", response.version());
//            log("First non blocking response body             : %s", response.body());
//            log("First non blocking response headers          : %s", response.headers().map());
//            log("First non blocking response status code      : %s", response.statusCode());
//        }).get();
//    }
}
