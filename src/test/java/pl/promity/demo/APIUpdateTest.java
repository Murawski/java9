package pl.promity.demo;

import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.function.IntSupplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static pl.promity.demo.Logger.log;

public class APIUpdateTest {

    @Test
    public void java9_optionalTest() {
        // optional as stream
        List<Integer> numbers = List.of(1, 2, 3, 4, 5, 6);
        Optional<Integer> presentNumber = Optional.of(7);
        Optional<Integer> emptyNumber = Optional.empty();

        String withPresentNumber = Stream.concat(numbers.stream(), presentNumber.stream())
                .map(String::valueOf)
                .collect(Collectors.joining(", "));
        log("Result with present optional        : %s", withPresentNumber);

        String withEmptyNumber = Stream.concat(numbers.stream(), emptyNumber.stream())
                .map(String::valueOf)
                .collect(Collectors.joining(", "));
        log("Result with empty optional          : %s", withEmptyNumber);



        // ifPresentOrElse callback
        emptyNumber.ifPresentOrElse(
                integer -> log("'ifPresentOrElse' present callback"),
                () -> log("'ifPresentOrElse' empty callback")
        );
        presentNumber.ifPresentOrElse(
                integer -> log("'ifPresentOrElse' present callback"),
                () -> log("'ifPresentOrElse' empty callback")
        );



        // or supplier
        emptyNumber.or(() -> Optional.of(LocalDateTime.now().getSecond())).ifPresent(
                integer -> log("Stored value                        : %s", integer)
        );
    }


    @Test
    public void java9_streamTest() {
        // iterate
        // fancy way to write for(int i = 0; i<20; i++) {}
        String numbers = Stream.iterate(0, in -> in < 20, in -> in + 1).map(String::valueOf).collect(Collectors.joining(", "));
        log("Generated numbers          : %s", numbers);




        // ofNullable
        String ofNullableStream = Stream.concat(Stream.of(1), Stream.ofNullable(null)).map(String::valueOf).collect(Collectors.joining(", "));
        log("Of nullable stream         : %s", ofNullableStream);




        List<String> names = List.of("Lanie Janusz","Shantell Allington",
                "Titus Kennerly","Virginia Pilon",
                "Ona Coletti","Normand Steffy",
                "Jess Pernice","Christena Fader",
                "Rolande Berardi","Melissa Antoine",
                "Louetta Isley","Jolyn Matsunaga",
                "Petronila Swaby","Rocio Palazzo",
                "Davida Marten","Matt Schepis",
                "Burton Bose","Francesco Kleine",
                "Shae Ackerson","Chas Hollister");

        // takeWhile
        String takeWhileNames = names.stream().takeWhile(name -> !name.startsWith("J")).collect(Collectors.joining(", "));
        log("Take while names           : %s", takeWhileNames);

        // dropWhile
        String dropWhileNames = names.stream().dropWhile(name -> name.startsWith("L")).collect(Collectors.joining(", "));
        log("Drop while names           : %s", dropWhileNames);

        // takeWhile&dropWhile
        String dropTakeWhileNames = names.stream()
                .dropWhile(name -> !name.startsWith("R"))
                .takeWhile(name -> !name.startsWith("D"))
                .filter( name -> name.charAt(1) == 'o')
                .takeWhile(name -> name.split(" ")[0].length() > 5)
                .collect(Collectors.joining(", "));
        // ZAGADKA!
        log("Drop/take while names      : %s", dropTakeWhileNames);

    }
}
