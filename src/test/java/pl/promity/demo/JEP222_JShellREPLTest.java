package pl.promity.demo;

import jdk.jshell.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static pl.promity.demo.Logger.*;

public class JEP222_JShellREPLTest {

    private JShell jShell;

    @Before
    public void setUp() {
        // more custom shell by JShell.builder()
        jShell = JShell.create();
    }

    @After
    public void tearDown() {
        jShell.close();
    }

    @Test
    public void java9_jSimpleExpressionEvaluation() {
        List<SnippetEvent> events = jShell.eval(" 3 + 3 * 2 + \"ala\" + PI");

        // check evaluation exceptions
        events.stream()
                .map(SnippetEvent::exception)
                .forEach(this::logJShellException);

        // check evaluation value
        events.forEach(this::logSnippetEvent);

        // check evaluation information
        events.stream()
                .map(SnippetEvent::snippet)
                .map(i -> (VarSnippet) i) // Snippet is abstract, cast to proper implementation
                .forEach(this::logVarSnippetData);


    }

    @Test
    public void java9_jShellEvents() {
        // fired on each snippet evaluation
        JShell.Subscription subscription = jShell.onSnippetEvent(event -> log("new snippet event %s", event));
        jShell.eval("2+2*2");

        // remove subscription if no longer needed
        jShell.unsubscribe(subscription);
        jShell.eval("3+3*3");

        // add event on jshell termination
        jShell.onShutdown(jShell -> log("jshell shutdown"));
    }

    @Test
    public void java9_jSimpleMethodEvaluation() {
        String fibonacci = "long fib(long a, long b, long max){" +
                                "if(a+b > max){" +
                                    "return b;" +
                                "}" +
                                "else{" +
                                    "return fib(b, a+b, max);" +
                                "}" +
                            "}";

        // pass method definition
        jShell.eval(fibonacci)
            .forEach(this::logSnippetEvent);

        // log defined methods in jShell
        jShell.methods()
                .forEach(this::logMethod);

        // execute method
        List<SnippetEvent> events = jShell.eval("fib(1,1,1_000_000_000)");

        // log events
        events
            .forEach(this::logSnippetEvent);

        // log snippets
        events.stream()
                .map(SnippetEvent::snippet)
            .forEach(this::logSnippet);

    }

    @Test
    public void java9_forwardReferencesTest() {
        String circleAreaMethod = "double area(double r){ return PI*r*r;}";

        // add method definition
        jShell.eval(circleAreaMethod);

        // execute method without PI
        log("-----------------------------------");
        log(" + Execute without PI");
        jShell.eval("area(2.0)")
            .forEach(this::logSnippetEvent);

        // define PI
        log("-----------------------------------");
        log(" + Define PI");
        jShell.eval("double PI = 3.14;")
            .stream().map(SnippetEvent::snippet)
            .forEach(this::logSnippet);

        // execute method with PI
        log("-----------------------------------");
        log(" + Execute with PI");
        jShell.eval("area(2.0)")
                .forEach(this::logSnippetEvent);
    }

    private void logMethod(MethodSnippet method) {
        log("-----------------------------------");
        log("Methods    :%s", method);
    }

    private void logSnippetEvent(SnippetEvent e) {
        log("-----------------------------------");
        log("Value      :%s", e.value());
        log("Status     :%s", e.status());
    }

    private void logJShellException(JShellException e) {
        log("-----------------------------------");
        log("Exceptions :%s", e);
    }

    private void logVarSnippetData(VarSnippet var) {
        log("-----------------------------------");
        log("Id         :%s", var.id());
        log("Type       :%s", var.typeName());
        log("Kind       :%s", var.kind());
        log("Name       :%s", var.name());
        log("Source     :%s", var.source());
    }

    private void logSnippet(Snippet var) {
        log("-----------------------------------");
        log("Id         :%s", var.id());
        log("Kind       :%s", var.kind());
        log("Source     :%s", var.source());
    }

}
