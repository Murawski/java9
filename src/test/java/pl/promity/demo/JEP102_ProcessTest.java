package pl.promity.demo;

import com.google.common.collect.Streams;
import org.junit.Test;

import java.io.*;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static pl.promity.demo.Logger.log;

public class JEP102_ProcessTest {

    @Test
    public void java9_getProcessInfoTest() throws IOException {
        // ping with 10 retries
        List<String> pingProcess = List.of("ping", "-c", "10", "wp.pl");

        Process process = new ProcessBuilder(pingProcess).start();
        log("Process PID    : %s", process.toHandle().pid());
        log("Process Info   : %s", process.toHandle().info());
    }

    @Test
    public void java9_onExitTest() throws Exception {
        // inheritIO will crash test when executing with maven-surefire
        // Process process = new ProcessBuilder("ping", "-c", "3",  "wp.pl").inheritIO().start();
        Process process = new ProcessBuilder("ping", "-c", "3",  "wp.pl").start();
        ProcessHandle processHandle = process.toHandle();

        // declare on exit future
        CompletableFuture<Void> userFuture = processHandle.onExit()
                .thenApply(ProcessHandle::info)
                .thenApply(ProcessHandle.Info::user)
                .thenAccept(user -> log("User       : %s", user));

        // declare on exit future
        CompletableFuture<Void> startTimeFuture = processHandle.onExit()
                .thenApply(ProcessHandle::info)
                .thenApply(ProcessHandle.Info::startInstant)
                .thenAccept(startInstant -> log("Start       : %s", startInstant));

        // declare on exit future
        CompletableFuture<Void> pidFuture = processHandle.onExit()
                .thenApply(ProcessHandle::pid)
                .thenAccept(pid -> log("PID         : %s", pid));

        // wait for finish
        userFuture.get();
        startTimeFuture.get();
        pidFuture.get();
    }

    @Test
    public void java9_currentProcessTest() {
        ProcessHandle processHandle = ProcessHandle.current();

        processHandle
                .info()
                .arguments()
                .stream()
                .flatMap(Stream::of)
                .forEach(
                        arg -> log("Current process arguments        : %s", arg)
                );
    }

    @Test
    public void java9_processListTest() {

        Stream<ProcessHandle> liveProcesses = ProcessHandle.allProcesses();
        String currentUserName = System.getProperty("user.name");

        liveProcesses
                // get my processes
                .filter(handle -> handle.info().user().orElse("").equals(currentUserName))
                // filter those without parent process
                .filter(handle -> handle.parent().isPresent())
                // filter those without child process
                .filter(handle -> handle.children().findAny().isPresent())
                .map(ProcessHandle::info)
                // get command line
                .map(ProcessHandle.Info::commandLine)
                .flatMap(Optional::stream)
                .map(String::valueOf)
                .forEach(process -> log("User process           : %s", process));
    }

    @Test
    public void java9_processPipingTest() throws Exception {

        // define processes list
        List<ProcessBuilder> builders = List.of(
                new ProcessBuilder("find", ".", "-type", "f"),
                new ProcessBuilder("xargs", "grep", "-h", "^import "),
                // original example used awk we use cut to get range of columns
                // new ProcessBuilder("awk", "{print $2;}"),
                new ProcessBuilder("cut","-d"," ","-f2-10"),
                // sort alphabetically and save only unique import
                new ProcessBuilder("sort", "-u"));
        // start processes pipeline
        List<Process> processes = ProcessBuilder.startPipeline(builders);

        // get overall results
        Process last = processes.get(processes.size()-1);
        try (InputStream is = last.getInputStream(); Reader isr = new InputStreamReader(is);
             BufferedReader r = new BufferedReader(isr)) {
            Streams.zip(r.lines(), IntStream.iterate(0, i -> i + 1).boxed(), (line, no) -> String.format("%3d",no)+" "+line)
                    .forEach(
                            line -> log("%s", line)
                    );
        }
    }


}
