package pl.promity.demo;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.*;

public class JEP269_CollectionTest {

    @Test
    public void pre_java9_immutableListTest(){
        List<String> modifiableList = new ArrayList<>();
        modifiableList.add("a");
        modifiableList.add("b");
        modifiableList.add("c");
        List<String> unmodifiableList = Collections.unmodifiableList(modifiableList);

        // what the...
        assertThat( unmodifiableList ).hasSize(3);
        modifiableList.add("d");
        assertThat( unmodifiableList ).hasSize(4);

        // nooo
        assertThatExceptionOfType(UnsupportedOperationException.class).isThrownBy(
                () -> unmodifiableList.add("d")
        );
    }

    @Test
    public void guava_immutableListTest(){
        List<String> unmodifiableList = ImmutableList.of("a", "b", "c");

        // nooo
        assertThatExceptionOfType(UnsupportedOperationException.class).isThrownBy(
                () -> unmodifiableList.add("d")
        );
    }

    @Test
    public void java9_immutableListTest(){
        List<String> unmodifiableList = List.of("a", "b", "c");

        // nooo
        assertThatExceptionOfType(UnsupportedOperationException.class).isThrownBy(
                () -> unmodifiableList.add("d")
        );
    }

    @Test
    public void java9_immutableListNullTest(){
        // nooo
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> List.of("a", "b", "c", null)
        );
    }

    @Test
    public void java9_immutableSetTest(){
        Set<String> unmodifiableSet = Set.of("a", "b", "c");

        // nooo
        assertThatExceptionOfType(UnsupportedOperationException.class).isThrownBy(
                () -> unmodifiableSet.add("d")
        );
    }

    @Test
    public void java9_immutableSetDuplicateTest(){
        // nooo
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> Set.of("a", "b", "c", "c")
        );
    }

    @Test
    public void java9_immutableMapTest(){
        Map<String, Integer> unmodifiableSet = Map.of("a", 1, "b", 2, "c", 3);
        // Map<String, Integer> unmodifiableSet = Map.ofEntries(Map.entry("a", 1), Map.entry("b", 2), Map.entry("c", 3));

        // nooo
        assertThatExceptionOfType(UnsupportedOperationException.class).isThrownBy(
                () -> unmodifiableSet.put("d", 4)
        );
    }


}
