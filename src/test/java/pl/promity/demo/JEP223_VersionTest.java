package pl.promity.demo;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;
import java.util.stream.Collectors;

import static pl.promity.demo.Logger.log;

public class JEP223_VersionTest {

    @Test
    public void java9_versionParseTest() {
        String version = "9.34.1-ga+11-" + LocalDate.now();

        Runtime.Version parse = Runtime.Version.parse(version);

        log("Input version       : %s", version);
        log("Major version       : %d", parse.major());
        log("Minor version       : %d", parse.minor());
        log("Security version    : %d", parse.security());
        log("Build version       : %d", parse.build().orElse(-1));
        log("Pre version         : %s", parse.pre().orElse("UNKNOWN"));
        log("Opt version         : %s", parse.optional().orElse("UNKNOWN"));
        log("Version string      : %s", parse.version().stream().map(String::valueOf).collect(Collectors.joining(".")));
    }


    @Test
    public void java9_versionComparisonTest() {
        // create long string version for today build
        String todayVersionText = "9.34.1-ga+11-" + LocalDate.now();
        Runtime.Version todayVersion = Runtime.Version.parse(todayVersionText);

        // create long string version for tomorrow build
        String tomorrowVersionText = "9.34.1-ga+11-" + LocalDate.now().plusDays(1);
        Runtime.Version tomorrowVersion = Runtime.Version.parse(tomorrowVersionText);

        // test comparison
        Assertions.assertThat(todayVersion.equals(tomorrowVersion)).isFalse();
        Assertions.assertThat(todayVersion.equalsIgnoreOptional(tomorrowVersion)).isTrue();

    }

    @Test
    public void java9_versionSystemPropertiesTest() {

        log("java.version                    : %s", System.getProperty("java.version", "UNKNOWN"));
        log("java.runtime.version            : %s", System.getProperty("java.runtime.version", "UNKNOWN"));
        log("java.vm.version                 : %s", System.getProperty("java.vm.version", "UNKNOWN"));
        log("java.specification.version      : %s", System.getProperty("java.specification.version", "UNKNOWN"));
        log("java.vm.specification.version   : %s", System.getProperty("java.vm.specification.version", "UNKNOWN"));

    }
}
