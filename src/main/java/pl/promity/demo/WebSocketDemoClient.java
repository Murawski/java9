package pl.promity.demo;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.WebSocket;

import java.net.URI;
import java.util.concurrent.*;

import static pl.promity.demo.Logger.log;

public class WebSocketDemoClient {

    private final HttpClient client;
    private WebSocket webSocket;

    public WebSocketDemoClient() {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        client = HttpClient.newBuilder().executor(executorService).build();
    }

    public void connect(URI address){
        webSocket = client.newWebSocketBuilder(
                address, new ListenerTest()).buildAsync().join();
    }

    public CompletableFuture<WebSocket> sendNonBlockingMessage(String hello) {
        return webSocket.sendText(hello, true);
    }

    public void closeSocket() throws ExecutionException, InterruptedException {
        webSocket.sendClose(WebSocket.NORMAL_CLOSURE, "BYE BYE").get();
        while (!webSocket.isClosed()) {}
    }


    private class ListenerTest implements WebSocket.Listener {

        @Override
        public void onOpen(WebSocket webSocket) {
            WebSocket.Listener.super.onOpen(webSocket);
            log("Opened web socket connection");
        }

        @Override
        public CompletionStage<?> onText(WebSocket webSocket, CharSequence message, WebSocket.MessagePart part) {
            WebSocket.Listener.super.onText(webSocket, message, part);
            log("Received web socket message %s", message);
            return CompletableFuture.completedStage(message);
        }

        @Override
        public CompletionStage<?> onClose(WebSocket webSocket, int statusCode, String reason) {
            WebSocket.Listener.super.onClose(webSocket, statusCode, reason);
            log("Closed web socket connection %s", reason);
            return CompletableFuture.completedStage(reason);
        }

        @Override
        public void onError(WebSocket webSocket, Throwable error) {
            WebSocket.Listener.super.onError(webSocket, error);
            log("Error in web socket %s", error);
        }
    }

}
