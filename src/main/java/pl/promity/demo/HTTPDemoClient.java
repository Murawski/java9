package pl.promity.demo;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.*;

public class HTTPDemoClient {

    private final HttpClient client;

    HTTPDemoClient() {
        ExecutorService executorService =
                Executors.newFixedThreadPool(2, new ThreadFactoryBuilder().setNameFormat("demo-pool-%d").build());
        // starts inner thread which consumes all connection events
        client = HttpClient.newBuilder().executor(executorService).build();

    }

    public HttpRequest createRequest(URI uri, String body) {
        return HttpRequest.newBuilder()
                .uri(uri)
                // can upgrade to http/2 but if server doesn't support http/2 it will fallback to http/1.1
                .version(HttpClient.Version.HTTP_2)
                // can specify timeout for given request
                .timeout(Duration.of(10, ChronoUnit.SECONDS))
                // can specify key value list of headers
                .headers("Content-Type", "text/plain;charset=UTF-8")
                .POST(HttpRequest.BodyProcessor.fromString(body))
                // other http methods
                // .POST()
                // .PUT()
                // .DELETE()
                .build();
    }

    public CompletableFuture<HttpResponse<String>> sendNonBlockingMessage(URI uri, String body){
        HttpRequest request = createRequest(uri, body);
        return client.sendAsync(request, HttpResponse.BodyHandler.asString());
    }

}
