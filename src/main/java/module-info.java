module java9demo {
    requires jdk.incubator.httpclient;
    requires com.google.common;
    requires jdk.jshell;

    exports pl.promity.demo;

}